/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";

import "tailwindcss/dist/base.min.css";
import tw, { styled } from "twin.macro"; // Tailwind

const Wrapper = styled.div`
  ${tw``}
`
const Footer = styled.footer`
  ${tw`text-center text-gray-800 pt-4 pb-4 mt-8 bg-gray-200`}
`

//bg-gray-500
const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)


  return (
    <Wrapper>
      <main>{children}</main>
      {/*<Footer>
        © {new Date().getFullYear()} <a href="https://www.gatsbyjs.com">Chamberlain Chefs</a>
      </Footer>*/}
    </Wrapper>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
