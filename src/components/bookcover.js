import React from "react";
import { useStaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

import tw, { styled } from "twin.macro"; // Tailwind


/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.com/docs/use-static-query/
 */


const ImageWrapper = styled.div`
  ${tw`shadow-2xl rounded-lg overflow-hidden mx-20 sm:mx-0`}
`
/*const BookCoverContainer = styled.div`
  ${tw`bg-gray-200 sm:bg-transparent max-h-full`}
`*/
const Image = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "chefs.png" }) {
        childImageSharp {
          fluid(maxWidth: 800) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  if (!data?.placeholderImage?.childImageSharp?.fluid) {
    return <div>Picture not found</div>
  }

  return( 
   
      <ImageWrapper>
        <Img fluid={data.placeholderImage.childImageSharp.fluid} />
      </ImageWrapper>
  
  )
}

export default Image
