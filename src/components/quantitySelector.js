import React from "react";
import tw, { styled } from "twin.macro"; // Tailwind

const ComponentWrapper = styled.div`
  ${tw`w-32 mb-4`}
`
const InputLabel = styled.label`
  ${tw`w-full text-gray-700 text-sm font-semibold`}
`
const InputWrapper = styled.div`
  ${tw`flex flex-row h-10 w-full rounded-lg relative bg-transparent mt-1`}
`
const ButtonDecrease = styled.button`
  ${tw`bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-l cursor-pointer focus:outline-none`}
  ${props =>
    (props.value == 1 ? tw`cursor-not-allowed` : 'test')}
`
const ButtonIncrease = styled.button`
  ${tw`bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-r cursor-pointer focus:outline-none`}
`
const Input = styled.input`
  ${tw`appearance-none outline-none focus:outline-none text-center w-full bg-gray-300 font-semibold text-base hover:text-black focus:text-black cursor-default flex items-center text-gray-700  focus:outline-none`}
`
/*const DecreaseSpan = styled.span`
  ${tw`text-transparent`}
  `*/

const QuantitySelector = (props) => {
 

  function handleIncrease(){
    props.onQuantityChange((props.value + 1));
  }

  function handleDecrease(){
    
    // We dont let the quantity go less than one
    if (props.value == 1 ) {
      return;
    }
   props.onQuantityChange((props.value - 1));
  }
  return (
    <ComponentWrapper>
      <InputLabel>Quantity
      </InputLabel>
      <InputWrapper>
        <ButtonDecrease value={props.value} dataAction="decrement" onClick={handleDecrease}>
          <span>−</span>
        </ButtonDecrease>
        <Input type="number" name="custom-input-number" value={props.value} readOnly></Input>
        <ButtonIncrease dataAction="increment" onClick={handleIncrease}>
          <span>+</span>
        </ButtonIncrease>
      </InputWrapper>
    </ComponentWrapper>
  )
}

export default QuantitySelector;