import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";
import BookCover from "../components/bookcover";
import QuantitySelector from "../components/quantitySelector";
import SEO from "../components/seo";
import tw, { styled } from "twin.macro"; // Tailwind

import ShoppingBag from "../images/shopping-bag.svg";

import { loadStripe } from '@stripe/stripe-js';

const stripePromise = loadStripe(`${process.env.GATSBY_STRIPE_PUBLISHABLE_KEY}`);

  const PageContainer = styled.div`
    ${tw`sm:flex sm:py-12 h-screen`}
  `
  const PageLeft = styled.div`
    ${tw`bg-gray-200 sm:bg-transparent flex-1 flex items-center justify-end`}
  `
  const PageContent = styled.div`
    ${tw`p-4 w-full xl:w-3/4`}
  `
  const PageRight = styled.div`
    ${tw`flex-1 flex items-center`}
  `
  const ButtonContainer = styled.div`
    ${tw`mx-8`}
  `
  const Button = styled.button`
    ${tw`shadow sm:w-auto w-full text-center bg-blue-600 hover:bg-blue-500 text-blue-100 font-bold py-4 px-4 rounded`}
  `
  const HeroText = styled.h1`
    ${tw`font-bold text-3xl mb-4`}
  `
  const Price = styled.h1`
    ${tw`text-xl text-gray-800 mb-4`}
  `
  const LeadText = styled.h2`
    ${tw`text-lg text-gray-900 mb-6 `}
  `

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { quantity: 1};
    this.handleQuantityChange = this.handleQuantityChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  handleQuantityChange(newQuantity){
    this.setState({quantity: newQuantity});
  }
  async handleClick(){

    const stripe = await stripePromise;
    const { error } = await stripe.redirectToCheckout({
      lineItems: [{
        price: `${process.env.GATSBY_STRIPE_PRODUCT_PRICE}`, // Replace with the ID of your price
        quantity: this.state.quantity,
      }],
      mode: 'payment',
      successUrl: 'https://www.chamberlainchefs.co.nz/thank-you',
      cancelUrl: 'https://www.chamberlainchefs.co.nz/',
      shippingAddressCollection: {
        allowedCountries: ['NZ'],
      }
    });
    // If `redirectToCheckout` fails due to a browser or network
    // error, display the localized error message to your customer
    // using `error.message`.
  };
  render() {
    return (
      <Layout>
        <SEO title="Not All Cooks Are Chefs" />
        <PageContainer>
          <PageLeft>
            <PageContent>
              <BookCover />
            </PageContent>
          </PageLeft>
          <PageRight>
            <PageContent>
              <HeroText>Not All Cooks Are Chefs</HeroText>
              <Price> $43.82</Price>
              <LeadText>
                <p>KidsCan mission is to provide the essentials to Kiwi kids affected by poverty so they can participate in learning and having an opportunity for a better future, https://www.kidscan.org.nz/, and the Chefs are keen to support them in this.</p>
                <br/>
                <p>It’s our aim to sell as many cookbooks as we can for this great cause. The breakdown is simple, $23 pays to print the book, and the remaining $20 goes to charity. </p>
              </LeadText>
              {/*<QuantitySelector value={this.state.quantity} onQuantityChange={this.handleQuantityChange}/>
              <Button role="link" onClick={this.handleClick}>
                Order Now
              </Button>*/}
            </PageContent>
          </PageRight>
        </PageContainer>
      </Layout>
    );
  }
}

export default IndexPage


