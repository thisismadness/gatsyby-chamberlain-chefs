import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";
import tw, { styled } from "twin.macro"; // Tailwind

const HeroText = styled.h1`
  ${tw`min-w-full text-center font-bold text-3xl px-8 pt-4`}
`
const HeroWrapper = styled.div`
  ${tw`h-screen flex items-center`}
`

const ThankYou = () => (
  <Layout>
    <SEO title="Thank You For Your Support" />
    <HeroWrapper>
      <HeroText>Thanks for your support!</HeroText>
    </HeroWrapper>
  </Layout>
)

export default ThankYou
